package middleware

import (
	"apiUser/utils"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/spf13/viper"
	"net/http"
	"strings"
)

func TokenVerifyMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authHeader := r.Header.Get("Authorization")
		bearerToken := strings.Split(authHeader, " ")

		if len(bearerToken) == 2 {
			authToken := bearerToken[1]

			token, err := jwt.Parse(authToken, func(token *jwt.Token) (i interface{}, err error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("[middleware.TokenVerifyMiddleware] Error when run token merhod with error: %w\n", err)
				}
				return []byte(viper.GetString("token.key")), nil
			})

			if err != nil {
				fmt.Printf("[middleware.TokenVerifyMiddleware] error when run jwt.Parse with error: %v\n", err)
				utils.HandleError(w, http.StatusUnauthorized, "Invalid Token")
				return
			}

			if token.Valid {
				next.ServeHTTP(w, r)
			} else {
				utils.HandleError  (w, http.StatusUnauthorized, "Ooops, something error")
				return
			}

		} else {
			utils.HandleError  (w, http.StatusUnauthorized, "Invalid Token")
			return
		}
	})
}
