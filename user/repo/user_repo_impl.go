package repo

import (
	"apiUser/model"
	"apiUser/user"
	"fmt"
	"github.com/jinzhu/gorm"
)

type UserRepoImpl struct {
	db *gorm.DB
}

func (u UserRepoImpl) UpdateAvatar(id int, avatar string) (*model.User, error) {
	var dataUser model.User
	err:= u.db.Model(&dataUser).Where("id = ?", id).Update("avatar", avatar).Error
	if err!= nil{
		return nil, fmt.Errorf("[UserRepoImpl.UpdateAvatar] Error when query update data with error: %w\n",err)
	}
	return &dataUser, nil
}

func (u UserRepoImpl) Register(data *model.User) (*model.User, error) {
	err:= u.db.Save(&data).Error
	if err!= nil{
		return nil, fmt.Errorf("UserRepoImpl.Register Error when query save data with: %w\n",err)
	}
	return data,nil
}

func (u UserRepoImpl) Login(data *model.User) (*model.User, error) {
	err:= u.db.Where("email=?",data.Email).Find(&data).Error
	if err!= nil{
		return nil, fmt.Errorf("[UserRepoImpl.Login] Error when query login with error: %w\n",err)
	}
	return data,nil
}

func (u UserRepoImpl) GetAll() (*[]model.User, error) {
	var data []model.User
	err:= u.db.Find(&data).Error
	if err!= nil{
		return nil, fmt.Errorf("[UserRepoImpl.GetAll] Error when query get all data with error: %w\n",err)
	}
	return &data,nil
}

func (u UserRepoImpl) GetByID(id int) (*model.User, error) {
	var data =model.User{}
	err:= u.db.First(&data,id).Error
	if err != nil {
		return nil, fmt.Errorf("UserRepoImpl.GetByID Error when query get by id with error: %w\n",err)
	}
	return &data,nil
}

func (u UserRepoImpl) GetByName(name string) (*[]model.User, error) {
	var data []model.User
	err:= u.db.Where("name=?",name).Find(&data).Error
	if err!= nil{
		return nil, fmt.Errorf("UserRepoImpl.GetByName Error when query get by name with error: %w\n",err)
	}
	return &data,nil
}

func (u UserRepoImpl) Update(id int, data *model.User) (*model.User, error) {
	err:= u.db.Model(&data).Where("id=?",id).Update(data).Error
	if err!= nil {
		return nil,fmt.Errorf("UserRepoImpl.Update Error when query update data with error: %w\n",err)
	}
	return data,nil
}

func (u UserRepoImpl) Delete(id int) error {
	data :=model.User{}
	err:= u.db.Where("id=?",id).Delete(&data).Error
	if err!= nil{
		return fmt.Errorf("[UserRepoImpl.Delete] Error when query delete data with error: %w\n",err)
	}
	return nil
}

func CreateUserRepoImpl(db *gorm.DB) user.UserRepo {
	return &UserRepoImpl{db}
}