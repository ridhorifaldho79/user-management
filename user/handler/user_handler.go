package handler

import (
	"apiUser/middleware"
	"apiUser/model"
	"apiUser/user"
	"apiUser/utils"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/badoux/checkmail"
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
	"golang.org/x/crypto/bcrypt"
)

type UserHandler struct {
	userUsecase user.UserUsecase
}

func CreateUserHandler(resp *mux.Router, userUsecase user.UserUsecase) {
	userHandler := UserHandler{userUsecase}

	resp.HandleFunc("/user", middleware.TokenVerifyMiddleware(userHandler.getAll)).Methods(http.MethodGet)
	//resp.HandleFunc("/user/{name}",userHandler.getByName).Methods(http.MethodGet)
	resp.HandleFunc("/user/{id}", middleware.TokenVerifyMiddleware(userHandler.getByID)).Methods(http.MethodGet)
	resp.HandleFunc("/user", userHandler.register).Methods(http.MethodPost)
	resp.HandleFunc("/user/{id}", middleware.TokenVerifyMiddleware(userHandler.update)).Methods(http.MethodPut)
	resp.HandleFunc("/user/{id}", middleware.TokenVerifyMiddleware(userHandler.delete)).Methods(http.MethodDelete)
	resp.HandleFunc("/user/login", userHandler.login).Methods(http.MethodPost)
	resp.HandleFunc("/user/avatar/{id}", middleware.TokenVerifyMiddleware(userHandler.updateAvatar)).Methods(http.MethodPut)
}

func (h UserHandler) getAll(writer http.ResponseWriter, request *http.Request) {
	dataUser, err := h.userUsecase.GetAll()
	if err != nil {
		fmt.Printf("[UserHandler.getAll] Error when request data to usecase with error: %v\n", err)
		utils.HandleError(writer, http.StatusInternalServerError, "Ooops, something error")
		return
	}
	utils.HandleSuccess(writer, http.StatusOK, dataUser)
}

func (h UserHandler) getByName(writer http.ResponseWriter, request *http.Request) {
	pathVar := mux.Vars(request)
	name := pathVar["name"]
	dataUser, err := h.userUsecase.GetByName(name)
	if err != nil {
		fmt.Printf("[UserHandler.getByName] Error when request data to usecase with error: %v\n", err)
		utils.HandleError(writer, http.StatusInternalServerError, "Ooops, something error")
		return
	}
	utils.HandleSuccess(writer, http.StatusOK, dataUser)
}

func (h UserHandler) getByID(writer http.ResponseWriter, request *http.Request) {
	pathVar := mux.Vars(request)
	id, err := strconv.Atoi(pathVar["id"])
	if err != nil {
		fmt.Printf("[UserHandler.getByID] Error when convert id to string with error: %v\n", err)
		utils.HandleError(writer, http.StatusBadRequest, "ID NOT VALID !!!")
		return
	}

	dataUser, err := h.userUsecase.GetByID(id)
	if err != nil {
		fmt.Printf("[UserHandler.getByID] Error when request data to usecase with error: %v\n", err)
		utils.HandleError(writer, http.StatusBadRequest, "ID DOES NOT EXIST")
		return
	}
	utils.HandleSuccess(writer, http.StatusOK, dataUser)
}

func (h UserHandler) register(writer http.ResponseWriter, request *http.Request) {

	var dataUser = model.User{}

	data, err := getData(request, dataUser, writer)
	if err != nil {
		fmt.Println("error")
		fmt.Printf("[UserHandler.register] Error when call function getData with error: %v\n", err)
		//utils.HandleError(writer,http.StatusInternalServerError, "Ooops, something error")
		return
	}
	if data == nil {
		fmt.Print("data nil")
		return
	}

	dataRegister, err := h.userUsecase.Register(data)
	if err != nil {
		fmt.Printf("[UserHandler.register] Error when send data to usecase with error: %v\n", err)
		utils.HandleError(writer, http.StatusInternalServerError, "Email already exist")
		return
	}
	utils.HandleSuccess(writer, http.StatusCreated, dataRegister)
}

func (h UserHandler) update(writer http.ResponseWriter, request *http.Request) {
	pathVar := mux.Vars(request)
	id, err := strconv.Atoi(pathVar["id"])
	if err != nil {
		fmt.Printf("[UserHandler.update] Error when convert id with error: %v\n", err)
		utils.HandleError(writer, http.StatusBadRequest, "ID NOT VALID !!!")
		return
	}

	_, err = h.userUsecase.GetByID(id)
	if err != nil {
		fmt.Printf("[UserHandler.update] Error when check id to usecase with error: %v\n", err)
		utils.HandleError(writer, http.StatusBadRequest, "ID DOES NOT EXIST")
		return
	}

	var data = model.User{}
	dataUser, err := getData(request, data, writer)
	if err != nil {
		fmt.Printf("[UserHandler.update] Error when call function getData with error: %v\n", err)
		//utils.HandleError(writer,http.StatusInternalServerError,"Ooops, something error")
		return
	}
	if dataUser == nil {
		return
	}

	dataUpdate, err := h.userUsecase.Update(id, dataUser)
	if err != nil {
		fmt.Printf("[UserHandler.update] Error when send data to usecase with error: %v\n", err)
		utils.HandleError(writer, http.StatusInternalServerError, "Email already exist")
		return
	}
	utils.HandleSuccess(writer, http.StatusOK, dataUpdate)
}

func (h UserHandler) delete(writer http.ResponseWriter, request *http.Request) {
	pathVar := mux.Vars(request)
	id, err := strconv.Atoi(pathVar["id"])
	if err != nil {
		fmt.Printf("[UserHandler.delete] Error when convert id with error: %v\n", err)
		utils.HandleError(writer, http.StatusBadRequest, "ID NOT VALID !!!")
		return
	}

	_, err = h.userUsecase.GetByID(id)
	if err != nil {
		fmt.Printf("[UserHandler.delete] Error when check id to usecase with error: %v\n", err)
		utils.HandleError(writer, http.StatusBadRequest, "ID DOES NOT EXIST")
		return
	}

	err = h.userUsecase.Delete(id)
	if err != nil {
		fmt.Printf("[UserHandler.delete] Error when request delete to usecase with error: %v\n", err)
		utils.HandleError(writer, http.StatusInternalServerError, "Ooops, something error")
		return
	}
	utils.HandleSuccess(writer, http.StatusOK, nil)
}

func (h UserHandler) login(writer http.ResponseWriter, request *http.Request) {
	var dataLogin = model.User{}
	var jwt = model.JWT{}

	err := json.NewDecoder(request.Body).Decode(&dataLogin)
	if err != nil {
		fmt.Printf("[UserHandler.login] Error when decoder data with error: %v\n", err)
		utils.HandleError(writer, http.StatusInternalServerError, "Ooops, something error")
		return
	}

	if dataLogin.Email == "" {
		utils.HandleError(writer, http.StatusBadRequest, "Email must be filled")
		return
	}

	if dataLogin.Password == "" {
		utils.HandleError(writer, http.StatusBadRequest, "Password must be filled")
		return
	}

	passInput := dataLogin.Password

	dataDB, err := h.userUsecase.Login(&dataLogin)
	if err != nil {
		fmt.Printf("[UserHandler.login] Error when request data to login usecase with error: %v\n", err)
		utils.HandleError(writer, http.StatusInternalServerError, "Invalid Email")
		return
	}

	isValid := utils.ComparePassword(dataDB.Password, []byte(passInput))
	if isValid {
		token, err := utils.GenerateToken(dataLogin)
		if err != nil {
			fmt.Printf("[UserHandler.login] Error when generate token with error: %v\n", err)
			utils.HandleError(writer, http.StatusInternalServerError, "Ooops,something error")
			return
		}
		jwt.Token = token
		utils.HandleSuccess(writer, http.StatusOK, jwt)
		return
	}
	utils.HandleError(writer, http.StatusBadRequest, "Invalid Password")
}

func (h UserHandler) updateAvatar(writer http.ResponseWriter, request *http.Request) {
	pathVar := mux.Vars(request)
	id, err := strconv.Atoi(pathVar["id"])
	if err != nil {
		fmt.Printf("[UserHandler.updateAvatar] Error when convert pathvar with error: %v\n", err)
		utils.HandleError(writer, http.StatusInternalServerError, "ID DOES NOT VALID !!!")
		return
	}

	_, err = h.userUsecase.GetByID(id)
	if err != nil {
		fmt.Printf("[UserHandler.updateAvatar] Error when check id to usecase with error: %v\n", err)
		utils.HandleError(writer, http.StatusBadRequest, "ID DOES NOT EXIST")
		return
	}

	avatar := uploadAvatar(writer, pathVar["id"], request)
	if avatar == "" {
		err := os.Remove(os.Getenv("PATH_ASSETS") + avatar)
		if err != nil {
			fmt.Printf("[UserHandler.updateAvatar] Error when remove data with error: %v\n", err)
			utils.HandleError(writer, http.StatusInternalServerError, "Ooops, something error")
			return
		}
	}

	dataUser, err := h.userUsecase.UpdateAvatar(id, avatar)
	if err != nil {
		fmt.Printf("[UserHandler.updateAvatar] Error when send data to usecase with error: %v\n", err)
		utils.HandleError(writer, http.StatusInternalServerError, "Oooops, something error")
		return
	}

	utils.HandleSuccess(writer, http.StatusCreated, dataUser)
}

func validation(writer http.ResponseWriter, data *model.User) bool {

	log.Println("'" + data.Name + "'")
	if data.Name == "" || strings.TrimSpace(data.Name) == "" {
		utils.HandleError(writer, http.StatusBadRequest, "Name must be filled in")
		return false
	}

	if data.Email == "" {
		utils.HandleError(writer, http.StatusBadRequest, "Email must be filled in")
		return false
	} else {
		err := checkmail.ValidateFormat(data.Email)
		if err != nil {
			fmt.Println(err)
			utils.HandleError(writer, http.StatusBadRequest, "Invalid Email")
			return false
		}
	}

	if data.Password == "" {
		utils.HandleError(writer, http.StatusBadRequest, "Password must be filled in")
		return false
	}

	if data.UserName == "" {
		utils.HandleError(writer, http.StatusBadRequest, "User Name must be filled in")
		return false
	}
	return true
}

func getData(request *http.Request, dataUser model.User, writer http.ResponseWriter) (*model.User, error) {
	err := json.NewDecoder(request.Body).Decode(&dataUser)
	if err != nil {
		return nil, fmt.Errorf("[UserHandler.getData] Error when decode data with error: %v\n", err)
	}

	valid := validation(writer, &dataUser)
	if valid == false {
		return nil, fmt.Errorf("[UserHandler.getData] unvalid data")
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(dataUser.Password), 10)
	if err != nil {
		return nil, fmt.Errorf("[UserHandler.getData] Error when generate password with error: %w\n", err)
	}

	dataUser.Password = string(hash)

	return &dataUser, nil
}

func uploadAvatar(writer http.ResponseWriter, id string, request *http.Request) string {
	uploadFileAvatar, handler, err := request.FormFile("avatar")
	if err != nil {
		fmt.Printf("[UserHandler.updateAvatar] Error when request from FormFile with error:%v\n", err)
		utils.HandleError(writer, http.StatusInternalServerError, "Avatar must be filled in")
		return ""
	}
	defer uploadFileAvatar.Close()

	var fileName = id + "-" + handler.Filename

	tempFileAvatar := filepath.Join(viper.GetString("file.path"), fileName)
	targetFile, err := os.OpenFile(tempFileAvatar, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		fmt.Printf("[UserHandler.updateAvatar] Error when open file with error: %v\n", err)
		utils.HandleError(writer, http.StatusInternalServerError, "Ooops, something error")
		return ""
	}
	defer targetFile.Close()

	if _, err := io.Copy(targetFile, uploadFileAvatar); err != nil {
		fmt.Printf("[UserHandler.updateAvatar] Error when copy file with error: %v\n", err)
		utils.HandleError(writer, http.StatusInternalServerError, "Ooops, something error")
		return ""
	}
	return viper.GetString("file.url_path") + fileName

}
