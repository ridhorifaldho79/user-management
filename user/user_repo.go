package user

import "apiUser/model"

type UserRepo interface {
	Register(data *model.User)(*model.User,error)
	Login(data *model.User)(*model.User,error)
	GetAll()(*[]model.User,error)
	GetByID(id int)(*model.User,error)
	GetByName(name string)(*[]model.User,error)
	Update(id int,data *model.User)(*model.User,error)
	Delete(id int)error
	UpdateAvatar(id int, avatar string)(*model.User,error)

}






