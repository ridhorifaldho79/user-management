package usecase

import (
	"apiUser/model"
	"apiUser/user"
)

type UserUsecaseImpl struct {
	userRepo user.UserRepo
}

func (u UserUsecaseImpl) UpdateAvatar(id int, avatar string) (*model.User, error) {
	return u.userRepo.UpdateAvatar(id, avatar)
}

func (u UserUsecaseImpl) Register(data *model.User) (*model.User, error) {
	return u.userRepo.Register(data)
}

func (u UserUsecaseImpl) Login(data *model.User) (*model.User, error) {
	return u.userRepo.Login(data)
}

func (u UserUsecaseImpl) GetAll() (*[]model.User, error) {
	return u.userRepo.GetAll()
}

func (u UserUsecaseImpl) GetByID(id int) (*model.User, error) {
	return u.userRepo.GetByID(id)
}

func (u UserUsecaseImpl) GetByName(name string) (*[]model.User, error) {
	return u.userRepo.GetByName(name)
}

func (u UserUsecaseImpl) Update(id int, data *model.User) (*model.User, error) {

	return u.userRepo.Update(id, data)
}

func (u UserUsecaseImpl) Delete(id int) error {
	return u.userRepo.Delete(id)
}

func CreateUserUsecase(userRepo user.UserRepo) user.UserRepo {
	return &UserUsecaseImpl{userRepo}
}
