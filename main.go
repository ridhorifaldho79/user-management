package main

import (
	"apiUser/driver"
	userHandler "apiUser/user/handler"
	userRepo "apiUser/user/repo"
	userUsecase "apiUser/user/usecase"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
	"log"
	"net/http"
)

func init()  {
	viper.SetConfigFile("config.json")
	if err:= viper.ReadInConfig();err!=nil{
		panic(err)
	}
}

func main()  {
	port:=viper.GetString("server.port")

	db,err:= driver.Connect()
	if err!= nil{
		fmt.Println("Error when connecting data base")
		log.Fatal(err)
	}
	defer db.Close()

	router:= mux.NewRouter().StrictSlash(true)

	userRepo:=userRepo.CreateUserRepoImpl(db)
	userUsecase:=userUsecase.CreateUserUsecase(userRepo)

	userHandler.CreateUserHandler(router,userUsecase)

	go serverImage()

	fmt.Println("Start web server at port "+port)
	err=http.ListenAndServe(":"+port,router)
	if err!= nil{
		fmt.Println("error when executed http.ListenAndServe")
		log.Fatal(err)
	}
}

func serverImage()  {
	fs := http.FileServer(http.Dir("./assets"))
	http.Handle("/", fs)

	port := "8083"
	fmt.Printf("Starting image server at http://localhost:%s/\n", port)
	log.Fatal(http.ListenAndServe(":" + port, nil))
}