package driver

import (
	"apiUser/model"
	"fmt"
	"log"
	"net/url"

	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
	"github.com/spf13/viper"
)

func Connect() (*gorm.DB, error) {
	//URL:=os.Getenv("URL")
	//db, err:= gorm.Open("postgres",URL)
	//if err!= nil{
	//	fmt.Println("[Driver.Connect]")
	//	log.Fatal(err)
	//	return nil,err
	//}

	dbHost := viper.GetString("database.host")
	dbPort := viper.GetString("database.port")
	dbName := viper.GetString("database.db_name")
	dbUser := viper.GetString("database.user")
	dbPass := viper.GetString("database.pass")

	connection := fmt.Sprintf("postgres://%s:%s@%s:%s/%s", dbUser, dbPass, dbHost, dbPort, dbName)

	val := url.Values{}
	val.Add("sslmode", "disable")

	URL := fmt.Sprintf("%s?%s", connection, val.Encode())

	db, err := gorm.Open("postgres", URL)
	if err != nil {
		fmt.Println("[Driver.Connect]")
		log.Fatal(err)
		return nil, err
	}

	db.Debug().AutoMigrate(
		&model.User{})
	return db, nil
}
