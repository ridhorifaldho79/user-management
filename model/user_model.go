package model

import "github.com/jinzhu/gorm"

type User struct {
	gorm.Model
	Name string `json:"name"`
	UserName string `json:"user_name"`
	Email string `gorm:"unique_index"json:"email"`
	Password string `json:"password"`
	Avatar string `json:"avatar"`
}

func (u User) TableName()string {
	return "tb_user"
}