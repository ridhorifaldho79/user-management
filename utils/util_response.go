package utils

import (
	"apiUser/model"
	"encoding/json"
	"fmt"
	"net/http"
)

func HandleSuccess(write http.ResponseWriter,status int, data interface{}) {
	resp:= model.Response{
		Status:  true,
		Message: "Success",
		Data:    data,
	}
	write.Header().Set("Content-Type","application/json")
	write.WriteHeader(status)

	err:= json.NewEncoder(write).Encode(resp)
	if err!= nil{
		write.WriteHeader(http.StatusInternalServerError)
		write.Write([]byte("Ooops, something error"))
		fmt.Printf("[HandleSuccess] Error when encode data with error: %v\n",err)
		return
	}
}

func HandleError(write http.ResponseWriter, status int, msg string)  {
	resp:= model.Response{
		Status:  false,
		Message: msg,
		Data:    nil,
	}
	write.Header().Set("Content-Type","application/json")
	write.WriteHeader(status)

	err:= json.NewEncoder(write).Encode(resp)
	if err!=nil{
		write.WriteHeader(http.StatusInternalServerError)
		write.Write([]byte("Ooops, something error"))
		fmt.Printf("[HandleError] Error when encode data with error: %v\n",err)
		return
	}
}