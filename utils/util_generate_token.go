package utils

import (
	"apiUser/model"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/spf13/viper"
)

func GenerateToken(user model.User)(string,error)  {
	secret:=viper.GetString("token.key")

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{

		"name":user.Name,
		"user_name": user.UserName,
		"email":user.Email,
	})
	tokenString,err:=token.SignedString([]byte(secret))
	if err!= nil{
		return "", fmt.Errorf("[utils.GenerateToken] Error when singnedString token with error: %v\n",err)
	}
	return tokenString,nil
}
