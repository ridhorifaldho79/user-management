package utils

import (
	"fmt"
	"golang.org/x/crypto/bcrypt"
)

func ComparePassword(hashedPassword string, password []byte) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), password)
	if err != nil {
		fmt.Printf("[ComparePassword] Error when CompareHashAndPassword with error: %v\n", err)
		return false
	}
	return true
}